# SemaPyre 
... is a simple terminal-based morse and semaphore trainer for Python3.

## Development stage
Early – stub level (files and directories existing but lacking desired content
yet)

## System Requirements
For this little program, an up-to-date Python3 interpreter as well as the
package `python3-pyaudio` have to be installed. If you are on a Linux, just type 

    sudo apt-get update 
    sudo apt-get upgrade
    sudo apt-get install python3
    sudo apt-get install python3-pyaudio

or `yum`, `pacman` etc. depending on what distribution you are using.

If you are on a different operating system, just ensure you have Python3 and
`pyaudio` for Python3 as described above.

## “Installation” and Run
To get this script running, simply type 

    git clone https://gitlab.com/das868/semapyre.git

inside the desired destination directory (in a terminal, of course). To run it, 
type one of the following

    python3 semapyre.py

within the that directory.
