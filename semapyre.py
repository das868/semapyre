# -*- coding: utf-8 -*-
# SemaPyre – a simple terminal-based morse and semaphore trainer for Python3
import math
from pyaudio import PyAudio


BITRATE = 16000 # #FPS ~ frameset
FREQUENCY = 500 # #Hz
LENGTH = 0.5    # #sec

NUMBEROFFRAMES = int(BITRATE * LENGTH)
RESTFRAMES = NUMBEROFFRAMES % BITRATE
WAVEDATA = ''

for x in range(NUMBEROFFRAMES):
    WAVEDATA += chr(int(math.sin(x / ((BITRATE / FREQUENCY)))))

# fill rest of frameset with silence
for x in range(RESTFRAMES):
    WAVEDATA += chr(128)


generator = PyAudio()
stream = generator.open(
    format=generator.get_format_from_width(1),
    channels=1,
    rate=BITRATE,
    output=True)
stream.write(WAVEDATA)
stream.stop_stream()
stream.close()
generator.terminate()
